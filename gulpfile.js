"use strict";

 var gulp = require('gulp'),
   concat = require('gulp-concat'),
   uglify = require('gulp-uglify'),
   rename = require('gulp-rename'),
     sass = require('gulp-sass'),
     maps = require('gulp-sourcemaps'),
     del = require('del'),
     browserSync = require('browser-sync');

//Объединяет все скрипты в один файл app.js
gulp.task("concatScripts", function() {
    return gulp.src([
        'js/jquery-3.3.1.min.js', 'js/blog-news.js',
        'js/grid-change.js', 'js/growing-images.js',
        'js/navigation.js', 'js/popup.js', 'js/slider.js'
        ])
    .pipe(maps.init())
    .pipe(concat('app.js'))
    .pipe(maps.write('./'))
    .pipe(gulp.dest('js'));
});

//Сжимает, полученный после concatScripts, файл app.js в файл app.min.js
gulp.task("minifyScripts", ["concatScripts"], function() {
  return gulp.src("js/app.js")
    .pipe(uglify())
    .pipe(rename('app.min.js'))
    .pipe(gulp.dest('js'));
});

//Компилирует все Sass файлы в css в одноименную папку
gulp.task('compileSass', function() {
  return gulp.src("scss/application.scss")
      .pipe(maps.init())
      .pipe(sass())
      .pipe(maps.write('./'))
      .pipe(gulp.dest('css'))
      .pipe(browserSync.reload({stream: true}));
});

//следит за изменениями в скриптах и sass
gulp.task('watchFiles', ['browser-sync'], function() {
  gulp.watch('scss/**/*.scss', ['compileSass']);
  gulp.watch('js/*', ['concatScripts']);
})

//удаляет файлы, при предыдущих сборках build
gulp.task('clean', function() {
  del(['dist', 'css/application.css*', 'js/app*.js*']);
});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browser Sync
        server: { // Определяем параметры сервера
            baseDir: './' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});

//Записывает нужные файл в продакшн папку dist
gulp.task("build", ['minifyScripts','compileSass'], function() {
  return gulp.src(["css/application.css", "css/normalize.css", 
                'index.html', 'products.html', "js/app.min.js", "img/**",
                   "fonts/**"], { base: './'})
            .pipe(gulp.dest('dist'));
});

//Запускает watch files
gulp.task('serve', ['watchFiles']);

//При выполнении команды gulp запускает таск clean, а затем build
gulp.task("default", ["clean"], function() {
  gulp.start('build');
});


//Как я понял, сначала запускаем serve, 
//чтобы gulp следил за изменениями в sass файлах и скриптах

//Потом, в самом конце выполняем gulp, который записывает 
//все нужные файлы в продакшн папку dist