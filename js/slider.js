var slideNow = 1;
var slideCount = $('#slidewrapper').children().length;
var slideInterval = 5000;
var navBtnId = 0;
var translateWidth = 0;
var lineWidth = 0;

$(document).ready(function() {
    //При загрузке страницы выставляет интервал и выполняет функцию nextSlide
    var switchInterval = setInterval(nextSlide, slideInterval);

    //При наведении на слайдер очищает интервал и слайдер не перелистывается
    $('#viewport').hover(function() {
        clearInterval(switchInterval);
    }, function() {     // Каждые 4с перелистывает слайды
        switchInterval = setInterval(nextSlide, slideInterval);
        }  
    );

    //при клике на навигацию получает индекс нажатого элемента
    $('.slide-nav-btn').click(function() {
        navBtnId = $(this).index();
//если этот индекс != текущему то выполняем следующее
        if (navBtnId + 1 != slideNow) {
        //высчитываем ширину сдвига - берем ширину слайда и умножаем на индекс элемента
            translateWidth = -$('#viewport').width() * (navBtnId);
            //Тоже самое делаем для двигающейся линии( + 9 добавляем за счет пэдингов)
            lineWidth = ($('.line').width() + 11) * (navBtnId);//ширина линии в навигации
            //смещаем наши слайды
            $('#slidewrapper').css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            //смещаем линию в навигации
            $('#active-line').css({
                'transform': 'translate(' + lineWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + lineWidth + 'px, 0)',
                '-ms-transform': 'translate(' + lineWidth + 'px, 0)',
            });
//для того чтобы ф-ия nextslide работала коректно выполняем следующее, так как slidenow 
//должно начинаться с единицы
            slideNow = navBtnId + 1;
        }
    });
});


function nextSlide() {
    if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
        $('#slidewrapper').css('transform', 'translate(0, 0)');
        $('#active-line').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        lineWidth = ($('.line').width() + 11) * (slideNow);
        $('#active-line').css({
                'transform': 'translate(' + lineWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + lineWidth + 'px, 0)',
                '-ms-transform': 'translate(' + lineWidth + 'px, 0)',
            });
        slideNow++;
    }
}
