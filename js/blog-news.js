//Text for blog titles
var text = ["What an Ecommerce theme!", "News about JavaScript", "Interesting facts about CSS"];
var text2 = ["Nice & clean. The best for your blog!", "Interesting facts about CSS", "News about JavaScript"];
//Text for blog contents
var text3 = ["Vivamus metus turpis, bibendum vitae sociosqu ad litora.", "Class aptent taciti sociosqu ad litora torquent.", "Lorem ipsum dolor sit amet, consectetur adipisicing elit."];
var text4 = ["Donec erat sem, vehicula id dictum sit vel, vulputate vel nibh.", "Lorem ipsum dolor sit amet, consectetur adipisicing elit.", "Class aptent taciti sociosqu ad litora torquent."];
var counter = 0;

var elem = document.getElementById("news-1-title");
$elem = $('#news-1-title');
$elem.delay(4000).fadeOut(500);
var elem2 = document.getElementById("news-2-title");
$elem2 = $('#news-2-title');
$elem2.delay(4000).fadeOut(500);
var elem3 = document.getElementById("news-1-content");
$elem3 = $('#news-1-content');
$elem3.delay(4000).fadeOut(500);
var elem4 = document.getElementById("news-2-content");
$elem4 = $('#news-2-content');
$elem4.delay(4000).fadeOut(500);


//Twitter elem


var twit = ["<span>@roymarvelous</span> You can seek a refund through TF if it is not as advertized - but it is:)", "<span>@ericafustero</span> Why thank you. Your work looks awesome by the way! <span>@treemelody</span>", "<span>@treemelody</span> Lorem ipsum dolor sit amet, consectetur"];
var twit2 = ["<span>@ericafustero</span> Why thank you. Your work looks awesome by the way! <span>@treemelody</span>", "<span>@treemelody</span> Lorem ipsum dolor sit amet, consectetur", "<span>@roymarvelous</span> You can seek a refund through TF if it is not as advertized - but it is:)"];
var twitDate = ["7 days ago", "15 days ago", "24 days ago"];
var twitDate2 = ["30 days ago", "3 days ago", "14 days ago"];


var elemTw = document.getElementById("first-twit");
var elemTw2 = document.getElementById("second-twit");

var elemTwDate = document.getElementById("twit-date-1");
var elemTwDate2 = document.getElementById("twit-date-2");

setInterval(change, 5000);

function change() {

  //News changes with fadeIn, fadeOut effect
  elem.innerHTML = text[counter];
  $elem.fadeIn(500).delay(3500).fadeOut(500);
  
  elem2.innerHTML = text2[counter];
  $elem2.fadeIn(500).delay(3500).fadeOut(500);

  elem3.innerHTML = text3[counter];
  $elem3.fadeIn(500).delay(3500).fadeOut(500);

  elem4.innerHTML = text4[counter];
  $elem4.fadeIn(500).delay(3500).fadeOut(500);

  // Twiter changes
  elemTw.innerHTML = twit[counter];
  elemTw2.innerHTML = twit2[counter];

  elemTwDate.innerHTML = twitDate[counter];
  elemTwDate2.innerHTML = twitDate2[counter];

  counter++;
  if (counter >= text.length) {
    counter = 0;
  }
}
